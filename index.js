
/* 

	Комментарии к тестовому заданию:
		- ВАЖНО! В Редисе необходимо включить генерацию событий кейспейсов. Самый простой способ выполнить "$ redis-cli config set notify-keyspace-events Kx"
		- Логика принятия сообщений не ясна из ТЗ. 
		  Я сделал так, что модуль получения обрабатывает последовательно одно сообщение за другим до тех пор, пока они есть.
		  Далее переходит в режим периодической проверки (500ms). 
		  Можно было также сделать подписку на появление нового сообщения, но в данном случае это не оправдано.
		- Получение всех сообщений при удалении реализовано порционно для работы с большим кол-вом сообщений. 
		- Функция генераций сообщений оставлена без изменений. Т.о. при смене генератора тело сообщения снова начинается с нуля. Я счел это нормальным поведением. 
		- Я не понял, какой Logger имелся ввиду в описании в тз, поиск по гиту не дал результатов, поэтому я использовал понравившийся мне - log4js
		- Код не покрыт тестами, так как я пока плохо знаком с этим. 
*/


var _ = require('underscore');
var log4js = require('log4js');
var logger = log4js.getLogger();
var redis = require("redis"),
    client = redis.createClient();
    sub_client = redis.createClient();

logger.setLevel('INFO');

client.on("error", function (err) {
    logger.error('Redis common error',err);
});

//Generator-part
var Generator = require("./generator");
var generator = new Generator(client);

//Receiver-part
var Receiver = require("./receiver");
var receiver = new Receiver(client);

//Stop recieving if i become a generator. and overwise start if generator-status off.
generator.on(Generator.ON_GENERATOR_STATUS_CHANGE,function(amIAGenerator){
	logger.info('Generator change a status! Its [%s] now',amIAGenerator?'on':'off');
	if(amIAGenerator){
		receiver.stopReceivingRoutine()
	}else{
		receiver.startReceivingRoutine()
	}
});

//We should wait for expiring the activeGenerator-key. If it happens, trying take a generator-place.
function subscribeToRedisForActiveGeneratorKeyExpiring(){
	sub_client.on('message',function(chan,msg){
		logger.debug('redis subsribe message recieved on channel[%s]: [%s]',chan,msg);
		if(msg === 'expired'){
			logger.info('We recieved Expired-event, so trying to become a generator...')
			tryStartAsGenerator();
		}
	});

	sub_client.subscribe('__keyspace@0__:activeGenerator');
}

//Try become a generator and become a Receiver if fails.
function tryStartAsGenerator(){
	generator.tryBecomeAGenerator(function(err,amIAGenerator){
		if(err){ 
			logger.error('tryBecomeAGenerator error');
		}else if(!amIAGenerator){
			logger.debug('tryStartAsGenerator fails. Becoming a receiver');
			if(!receiver.isReceiver){receiver.becomeAReceiver();}
		}
	});
}


//Print and delete all messages procedure. Called via CLI
function printAndDeleteErrors(){
	receiver.printAndDeleteErrors(function(err,msgs){
		if(err){
			logger.error('printAndDeleteErrors failed',err);
		}else{
			logger.info('All messages (%s) has been deleted. Thats what they were:',msgs.length);
			_(msgs).each(function(item){logger.info(item);})
		}
	});
}

//Subscribing to generator-key expiring event
subscribeToRedisForActiveGeneratorKeyExpiring();

//Processing CLI arguments and start printering and deleting procedure. Or start normal case overwise
if(process.argv[2] && process.argv[2]=='getErrors'){
	printAndDeleteErrors();
	return;
}else{
	tryStartAsGenerator();
}
