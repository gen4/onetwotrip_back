
var Redis = require('ioredis');
var redis = new Redis();

//определяем уникальый индификатор экземпляра. Я использую ProcessID, но в случае запуска на разных серверах, нужно использовать какой-либо другой. 
var uniqueInstanceId = process.pid;


//subsribe to 
redis.set('activeGenerator', uniqueInstanceId);
redis.get('activeGenerator', function (err, result) {
  console.log(result);
});
