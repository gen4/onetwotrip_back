var async = require('async');
var log4js = require('log4js');
var logger = log4js.getLogger();

Receiver = function(redis){
	logger.debug('reciever inited');	
	//receiving period in ms
	this.receivingPeriod = 500;
	this.isReceiver = false;
	this.redis = redis;
}

//Try to get ALL messages one by one until message stack is empty. 
//Then starting periodic receiving routine.
Receiver.prototype.becomeAReceiver  = function(){
	var that = this;
	//if reciever already run, ignore it (possible while generator change)
	if(this.isReceiver){logger.warn('double receiver run'); return;}
	this.isReceiver = true;
	
	//closure for getting messages one be one until recive empty message 
	function getInstanceMessageCallback(err,message){
		if(err){
			logger.error('becomeReceiver. getNewMessage error',err);
		}else if(!message){
			logger.debug('Messages stack is empty now. Starting periodic recieving')
			that.startReceivingRoutine();
		}else{
			//proccess recieved message and try get next
			that.processMessage(message);
			that.getNewMessage(getInstanceMessageCallback);
		}
	}
	this.getNewMessage(getInstanceMessageCallback);
}

//Start periodic receiving
Receiver.prototype.startReceivingRoutine = function(){
	
	logger.info('Recieving routine has been started')
	var that = this;
	this.intervalId = setInterval(
		function(){
			that.getNewMessage(that.recievingCallback.bind(that));
		},
		this.receivingPeriod);
}

//Stop receiving
Receiver.prototype.stopReceivingRoutine = function(){
	this.isReceiver = false;
	clearInterval(this.intervalId);
};


//Recive and delete one message from redis
Receiver.prototype.getNewMessage = function(done){
	logger.debug('try to get new message...');
	this.redis.LPOP('messages',done);
}


//Callback for receiving message
Receiver.prototype.recievingCallback = function(err,msg){
	if(err){
		logger.error(err);
	}else if(!msg){
		logger.warn('message hasnt received')
	}else{
		logger.debug('recieved message [%s]',msg);
		this.processMessage(msg);
	}
}

//Print and delete all messages from server
//We working with chunks of data, avoiding processing large amounts of messages at one time.
Receiver.prototype.printAndDeleteErrors = function(done){
	//сообщения из БД следует брать не все, а порционно, на случай, если их там слишком много.
	var messages = [];
	var message_counter=0;
	var chunk_size = 100; 
	var that = this;

	function getChunk(err,messages_part){
		logger.debug('get [%s]st chunk. Received [%s] messages. Now [%s] messages in local array.',message_counter,messages_part.length,messages.length);
		messages = messages.concat(messages_part);
		message_counter++;
		if(err){
			callback(err)
		}else if(!messages_part || messages_part.length===0 || messages_part.length<chunk_size) {
			//after receiving all messages - delete all of them on redis.
			logger.debug('all messages received. Now deleting...');
			that.redis.DEL('messageHistory',function(err){
				logger.debug('All messages deleted')
				done(err,messages);		
			});
		}else{
			//There is more messages in server, get new chunk
			logger.debug('Get new chunk...')
			that.redis.LRANGE('messageHistory',messages.length,messages.length+chunk_size,getChunk);	
		}	
	}
	//TODO this should be refactored maybe
	that.redis.LRANGE('messageHistory',messages.length,messages.length+chunk_size,getChunk);		
}

//Fake proccess message function
Receiver.prototype.processMessage = function(msg){
	logger.info('Message processed: %s',msg);
}

module.exports = Receiver;