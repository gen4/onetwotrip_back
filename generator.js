
var async 			= require('async');
var logger 			= require('log4js').getLogger();
var util         	= require("util");
var EventEmitter 	= require("events").EventEmitter;


Generator = function(redis){	
	//определяем уникальый индификатор экземпляра. Я использую ProcessID, но в случае запуска на разных серверах, нужно использовать какой-либо другой. 
	this.uniqueInstanceId = process.pid;
	
	this.redis = redis;
	this.isGenerator = false;
	
	//Pause between message generations
	this.generationPeriod = 500;

	//Время жизни записи о текущем генераторе
	this.activeGeneratorKeyTTL = 500;

	//Переодичность обновления записи о текущем генераторе (не должно быть больше activeGeneratorKeyTTL)
	this.activeGeneratorKeyRenewPeriod = this.activeGeneratorKeyTTL/2;
}

//Inherits event prototype
util.inherits(Generator, EventEmitter);

//Static vars
Generator.ON_GENERATOR_STATUS_CHANGE = 'onGeneratorStatusChange';



//Функцию отправки сообщений оставляю без изменения.
Generator.prototype.getMessage = function(){
	this.cnt = this.cnt || 0;
	logger.debug('Generator [%s]: message [%s] has been sent',this.uniqueInstanceId,this.cnt);
	return this.cnt++;
}

//Generator periodic functions starts here
Generator.prototype.runGenerator = function(){
	logger.info('Generator instance [%s] has been started',this.uniqueInstanceId);
	var that = this;
	
	//set periodic call of id-renew in redis
	this.renewIntervalID = setInterval(function(){
		that.renewCurrentGenerator(that.generatorRenewCallback);
	},this.activeGeneratorKeyRenewPeriod);

	//set periodic call of message-generation function
	this.msgIntervalID = setInterval(function(){
		that.sendMessage(that.getMessage(),that.generatorMsgCallback);
	},this.generationPeriod);

	this.emit(Generator.ON_GENERATOR_STATUS_CHANGE,true);
}



//Renew the currentGenerator-line in Redis. 
//If it is not equal to mine id,  we stop generation and throw critical error
Generator.prototype.renewCurrentGenerator = function(done){
	logger.debug('renew current gen. id...',this.uniqueInstanceId,this.activeGeneratorKeyTTL)
	this.redis.eval(Generator.luaRenewScript,2,'activeGenerator', 'ttl', this.uniqueInstanceId,this.activeGeneratorKeyTTL,done);	
}

//Sending message to redis
Generator.prototype.sendMessage = function(message,done){
	logger.info('sending message [%s]',message);
	this.redis.eval(Generator.saveMessageScript,1,'messageBody', message,done);	
	// this.redis.LPUSH('messages',message,done);
}

//set app id to activeGenerator field with TTL and ONLY 	
Generator.prototype.setCurrentGeneratorId = function(done){
	logger.info('seting current gen. id...',this.uniqueInstanceId,this.activeGeneratorKeyTTL)
	var args = ['activeGenerator',this.uniqueInstanceId, 'PX', this.activeGeneratorKeyTTL, 'NX'];
	this.redis.set(args, done);
} 


//Try to set this app_id to generator field, and check if it set already.
Generator.prototype.tryBecomeAGenerator = function(done){
	var that = this;
	logger.debug('Trying become a generator...');
	this.setCurrentGeneratorId(function(err,isSet){
		if(err){
			done(err);
		}else if(isSet=='OK'){
			logger.info('Now iam a generator');
			that.isGenerator = true;
			that.runGenerator();
		}else{
			logger.info('Iam not a generator for now. Setting fails.');
//			that.subscribeToCurrentGeneratorExpired();
		}
		done(null,isSet=='OK');
	});
};

//Stop all generator periodic functions
Generator.prototype.stopGenerator = function(){
	clearIntervl(this.msgIntervalID);
	clearIntervl(this.renewIntervalID);
	this.isGenerator = false;
	this.emit(Generator.ON_GENERATOR_STATUS_CHANGE,false);
	//try to become a generator again and subscribe to generator-fail event.
	this.tryBecomeAGenerator()
}


Generator.prototype.subscribeToCurrentGeneratorExpired = function(){
	//TODO
}

//Callback for renew generator-id-line in redis
Generator.prototype.generatorRenewCallback = function(err,isRenewed){
	logger.debug('generatorRenewCallback',err,isRenewed);
	var that = this;
	if(err){
		throw err;
	}else if(!isRenewed){
		//stop generator
		logger.error('CurrentGenerator-line renew failed.');
		that.stopGenerator();
	}else{
		logger.trace('Generator id has been updated')
	}
}

//Callback for sending message to redis
Generator.prototype.generatorMsgCallback = function(err,result){
	if(err){
		logger.error(err);
		throw err;
	}
}
// Script for renew generatorId line in Redis. Its set new value if its euqal with my id. Second arg - is TTL in ms.
Generator.saveMessageScript = 
		'redis.call("LPUSH","messages", ARGV[1]) '+
		'return redis.call("LPUSH","messageHistory", ARGV[1])'
		


// Script for renew generatorId line in Redis. Its set new value if its euqal with my id. Second arg - is TTL in ms.
Generator.luaRenewScript = 
	'if redis.call("get",KEYS[1]) == ARGV[1] '+
	'then '+
		'redis.call("set",KEYS[1], ARGV[1],"PX", ARGV[2]) '+
		'return 1 '+
	'else '+
		'return 0 '+
	'end';


module.exports = Generator;